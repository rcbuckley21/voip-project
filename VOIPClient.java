package finalProject;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;

/**a peer to peer client for VOIP.
 * uses a MulticastSocket so that any number of clients is
 * able to connect to the voice chat.
 * @author Cullen Buckley
 */

public class VOIPClient	{
	
	private static final int PORT = 4455;
	//private static final String SEND_ADDRESS = "10.0.0.201"; //change this to my local to connect them
	private static AudioFormat format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 44100, 16, 2, 4, 44100, false);
	private static DatagramSocket sendSocket;
	private static MulticastSocket recSocket;
	private static InetAddress group;
	private static String myAddress;
	
	public static void main(String[]args) throws IOException, LineUnavailableException	{
		myAddress = InetAddress.getLocalHost().getHostAddress();
		connect(); openMic(); receiveVoice();
	}
	public static void connect() throws UnknownHostException	{
		//arbitrarily choose a multicast group to connect to
		group = InetAddress.getByName("224.0.1.1");
	}
	public static void openMic() throws LineUnavailableException, IOException	{
		//opens a dataline for microphone input, that input is then sent over a datagram socket 
		DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
		TargetDataLine mic = (TargetDataLine)AudioSystem.getLine(info);
		mic.open();
		mic.start();
		byte[] buffer = new byte[2048];
		sendSocket = new DatagramSocket();
		Thread open = new Thread()	{
			@Override
			public void run()	{
				while(true)	{
					mic.read(buffer, 0, buffer.length);
					DatagramPacket packet = new DatagramPacket(buffer, buffer.length, group, PORT);
					try {
						sendSocket.send(packet);
					} 
					catch (IOException e) {}
				}
			}
		};
		open.start();
	}
	public static void receiveVoice() throws LineUnavailableException, IOException	{
		//opens a dataline for the systems speaker to output sound, receives datagrams to play to the dataline
		DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
		SourceDataLine speaker = (SourceDataLine)AudioSystem.getLine(info);
		speaker.open();
		speaker.start();
		byte[] buffer = new byte[2048];
		recSocket = new MulticastSocket(PORT);
		recSocket.joinGroup(group);
		Thread receive = new Thread()	{
			@Override
			public void run()	{
				while(true)	{
					DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
					try {
						recSocket.receive(packet);
						//do not write packets from yourself to your output device.
						//this stops you from having to listen to yourself
						if(!packet.getAddress().equals(InetAddress.getByName(myAddress))) {
							//System.out.println(packet.getAddress().getHostAddress()); 
								//used to test if receiving packets from others 
							speaker.write(buffer, 0, buffer.length);
						}
					} 
					catch (IOException e) {}
				}
			}
		};
		receive.start();
	}
}